This repository contains my solutions for the [python challenge](https://pythonchallenge.com). I will try to do at least one problem per day and we'll see if I can keep it up till problem 33. Even though I wish to master vim, I will use the Jupyter Notebook for this project/challenge.

### Warning: ###  
You will find my thought process for the solution ahead, so if you want to try the challenge yourself, stop here. If you want some hints, this is a good place. For complete solution, see the jupyter notebook file. 

#### Problem 0: ####  
   Pretty straightforward, just print the result of the mathematical expression and replace 0 in the url with the result. 

#### Problem 1: ####
   My first instinct was to replace 'k' with 'm', 'o' with 'q', and 'e' with 'g'. I did it and printed the text but it was still gibbirish. I thought maybe my program wasn't working so I opened the docs and read the documentation for replace in string. It was correct, it had to work but it didn't.
    Then, It struck my mind, the maping was obvious. I created a loop and mapped every character and done. Pretty simple still once you get the idea. The str.maketrans way to map characters was discovered only after doing it my way.
    
#### Problem 2: ####
   So, I inspected the webpage and found the huge comment that said find the rare characters. I created a defaultdict and counted the occurance of every character in the text. A word was formed from the rare characters, I put that into the url and done. Not that hard.

#### Problem 3: ####
   There was no clue this time, so I went ahead and inspected the webpage to find yet another large blob of text. One small letter surrounded by exactly three big bodyguards, sounded like a small letter between three big capital letter. So, I whipped up a small regex matcher, tons of matches found. Noticed the focus on exactly, so modified to make sure exactly 3 caps before and after, still multiple matches found. 
   Stalled for a while and took some time to realize that only small letters are important and done. A little complicated without regex but still pretty easy with regex.
   
#### Problem 4: ####
   This took more time than expected. As soon as I saw the problem, I looked at the inspector(I already had it open). Use urllib, so I searched for some link, found the nothing parameter link, so followed the link repeatedly until I got the message. I made the script to follow the instructions, and had to change it multiple times for adjustments and had to start from beginning. Network isn't the fastest here, so took some time to test it every time. Finally figured out a way to continue from where program would run into error(split the jupyter cell , duh) At the 250th iteration, got the next problem. yay!
   It wasn't that hard, but I learnt urllib.
   
#### Problem 5: ####
   It has been a habit to look at the inspector. The hints were not helpful. But, I found a file in the webpage. I opened the link, downloaded it and printed the file. Bunch of rubbish. Maybe it's a binary file, again printed the file in binary format. I'm not a machine to understand it, so converted to utf-8, no luck. Again scoured through some other links to come across the pickle module. peakhell, pickle sounded familiar. Looked at the docs and dumped the binary file to get a list, which doesn't make much sense. 
   Looking a bit more closely, the data consisted of rows which consisted of tuples with a character and a number. Maybe an ecoded form of something. Realized the character were only '#' and whitespaces. So, took some time to realize that it was a drawing on a console like the donut in C. It was pretty straight forward from then. 
   Programmatically not that hard, maybe I'm in the wrong place, looking to practice python but I'm enjoying the problems. :)